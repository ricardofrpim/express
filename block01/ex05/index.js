const express = require('express');
const app = express();
const port = 4040;

app.listen(port, ()=>{
	console.log(`server is running on port port ${port}`)
})

const accounts = {};

app.get('/account/new/:accountID/:amount', (req, res) =>{
	const {accountID, amount} = req.params;
    
	if (!(accountID in accounts)) {   	
    accounts[accountID]= parseInt(amount)
		res.send({ ok: true, data: `account ${accountID} created with ${amount} euros` })	
	} else {
		res.send({ ok: true, data: `account ${accountID} already exists` })			
	}	
})

app.get('/:accountID/withdraw/:amount', (req, res) =>{
	const {accountID, amount} = req.params;

	if (!(accountID in accounts)) {    	
		res.send({ ok: true, data: 'Account not found' })
	} else {
		accounts[accountID] -= parseInt(amount)
		res.send({ ok: true, data: `${amount} euros taken from account num ${accountID}` })		
	}			
})

app.get('/:accountID/deposit/:amount', (req, res) =>{
	const {accountID, amount} = req.params;

	if (!(accountID in accounts)) {    	
		res.send({ ok: true, data: 'Account not found' })
	} else {
		accounts[accountID] += parseInt(amount)
		res.send({ ok: true, data: `${amount} euros added to account num ${accountID}` })		
	}			
})

app.get('/:accountID/balance', (req, res) =>{
	const {accountID} = req.params;

	if (!(accountID in accounts)) {    	
		res.send({ ok: true, data: 'Account not found' })
	} else {
		const balance = accounts[accountID]
		res.send({ ok: true, data: `${balance}` })		
	}			
})

app.get('/:accountID/delete', (req, res)=> { 
	const {accountID} = req.params;

	if (!(accountID in accounts)) {    	
		res.send({ ok: true, data: 'Account not found' })
	} else {
		delete accounts[accountID]
		res.send({ ok: true, data: `Account num ${accountID} deleted` })		
	}			
})	

app.get('*', (req, res)=> {
    res.send({ ok: true, data: `404 resource not found` })
})

console.log(accounts)