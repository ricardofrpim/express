const express = require('express');
const app = express();
const port =4040;

app.listen(4040, ()=>{
	console.log(`server is running on port port ${port}`)
})	

let languages = {
        NL: 'Hallo Wereld',
        IT: 'Ciao Mondo',
        asdf: 'Hello World',
        EN: 'Hello world',
    };

app.get('/', (req, res)=>{
	res.send({ ok: true, data: 'Hello World' })
})

app.get('/:language', (req, res)=>{
	let {language} = req.params;

	if(language in languages) {
		res.send({ ok: true, data: languages[language] })
	} else {
		res.send({ ok: true, data: 'Hello World in ' + language + ' not found' })
	}
})

app.get('/:language/:message', (req, res)=>{
	let {language, message} = req.params;

	if (!(language in languages)) {
	languages[language] = message;
		res.send({ ok: true, data: language + ' added with message ' + message })
	}	
})
