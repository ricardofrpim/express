const express = require('express')
const app = express()

const port = process.env.PORT || 4040

app.get('/', (req, res)=>{
	res.send({ ok: true, data: 'Hello World' })
})

app.get('/:data', (req, res)=>{
	let greeting = req.params.data;

	if (greeting === 'NL') { 
		res.send({ ok: true, data: 'Hallo Wereld' }) 
	} else if (greeting === 'IT') {		
		res.send({ ok: true, data: 'Ciao Mondo' })
	} else if (greeting === 'asdf') {
		res.send({ ok: true, data: 'Hello World' })
	} 
})

app.get('/:data', (req, res)=> {

})

app.listen(4040, ()=>{
	console.log('serving my master on port ', port)
})	


// ====ex01====

// app.get('/', (req, res)=>{
// 	res.send({ ok: true, data: 'Hello World' })
// })

// app.get('/:data', (req, res)=>{
// 	let greeting = req.params.data;

// 	if (greeting === 'NL') { 
// 		res.send({ ok: true, data: 'Hallo Wereld' }) 
// 	} else if (greeting === 'IT') {		
// 		res.send({ ok: true, data: 'Ciao Mondo' })
// 	} else if (greeting === 'asdf') {
// 		res.send({ ok: true, data: 'Hello World' })
// 	}
// })