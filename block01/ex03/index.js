const express = require('express')
const app = express()

const port = process.env.PORT || 4040

app.listen(4040, ()=>{
	console.log('serving my master on port ', port)
})	

let languages = {};

app.get('/:language', (req, res)=>{
	let {language} = req.params;

	if(!(language in languages)) {
		res.send({ ok: true, data: 'Hello World in ' + language + ' not found' })
	} else {
		res.send({ ok: true, data: languages[language] })
	}
})

app.get('/:language/remove', (req, res)=>{
	let {language} = req.params;	
	delete languages[language]
		res.send({ ok: true, data: language + ' removed'})		
})

app.get('/:language/:message', (req, res)=>{
	let {language, message} = req.params;	
	if (!(language in languages)) {
	 languages[language] = message;
		res.send({ ok: true, data: language + ' added with message ' + message })
	}		
})

