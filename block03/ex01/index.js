// =============== IMPORT AND RUN EXPRESS AND ROUTES
const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    categoriesRoute = require('./routes/CategoriesRoute'),
    productsRoute = require('./routes/ProductsRoute'),    
    bodyParser = require('body-parser');
// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect('mongodb://127.0.0.1/products2022')
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// end of connecting to mongo and checking if DB is running

// using routes
app.use('/category', categoriesRoute);
app.use('/product', productsRoute);

// Set the server to listen on port 3000
app.listen(4040, () => console.log(`listening on port 3000`))