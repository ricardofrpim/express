const express = require('express') 
	router = express.Router()
	controller = require('../controllers/category');

// =============== ADD NEW CATEGORY TO DB ===============
router.post('/add', controller.add);

// =============== REMOVE ONE CATEGORY BY ID ===============
router.post('/delete', controller.delete);

// =============== UPDATE ONE CATEGORY BY ID ===============
router.post('/update', controller.update);

// =============== FIND ALL CATEGORIES ===============
router.get('/categories', controller.findAllCategories);

// =============== FIND ALL PRODUCTS BY CATEGORY ===============
router.get('/:category', controller.findAllProdsByCat);

module.exports  = router;