const express = require('express') 
	router = express.Router();
	controller = require('../controllers/product');

// =============== ADD NEW PRODUCT TO DB ===============
router.post("/add", controller.add);

// =============== DELETE PRODUCT BY ID ===============
router.post("/delete", controller.delete);

// =============== UPDATE ONE PRODUCT BY ID ===============
router.post('/update', controller.update);

// =============== FIND ALL CATEGORIES WITH ALL PRODUCTS ===============
router.get('/', controller.find);

// =============== FIND ONE PRODUCT BY NAME IN BODY ===============
router.get('/:product', controller.findOne);

module.exports = router;
