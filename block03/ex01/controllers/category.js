const Categories = require('../models/CategoriesModel');

class CategoryController {
    
    // ADD CATEGORY
    async add (req, res){
    let { category } = req.body;

    try {
        const found = await Categories.findOne({ category });
        if(found){
         	res.send({ ok: true, data: `Category ${category} already exist` });
        }else{
         	const addCategory = await Categories.create({ category });
        	res.send({ ok: true, data: `Category ${category} added successfully` });
        }
      }
      catch(error){
        res.send({ error });
      };
    }

    // DELETE CATEGORY
    async delete (req, res){
   	let {category} = req.body;

    try {
        const found = await Categories.findOne({ category });
        if(found){
	    	const removeCategory = await Categories.deleteOne({ category });
	    	res.send({ ok: true, data: `Category ${category} deleted successfully` });          
        }else{
        	res.status(200).send({ ok: true, data: `Category ${category} doesn't exist`});        	    	
        }
      }
      catch(error){
        res.send({ error });
      };
    }	    

    // UPDATE CATEGORY
    async update (req, res){
    let {category, newCategory} = req.body;

    try {
        const found = await Categories.findOne({ category });
        if(found){
	        const updateCategory = await Categories.updateOne( { category },{ category: newCategory } )
        	res.status(200).send({ ok: true, data: `Category ${category} updated successfully` });        	    		                 
        }else{
        	res.status(200).send({ ok: true, data: `Category ${category} doesn't exist`});        	    	
        }
      }
      catch(error){
        res.send({ error });
      };
    }	  

    // FIND ALL CATEGORIES
    async findAllCategories (req, res){    	

        try {
            const findCategories = await Categories.find({});
            const categories = findCategories.map(elem => elem.category);            
            res.send({ ok: true, data: categories.toString() });
        }
        catch (error){
            res.send({ error });
        };
    }

     //FIND ALL PRODUCTS BY CATEGORY
    async findAllProdsByCat (req ,res){
        let { category } = req.params;

    try {
        const found = await Categories.findOne({ category });
        if(found){
            const allProdCat = await Categories.find({category});
            res.send({ ok: true, data: allProdCat });        
        }else{
        	res.status(200).send({ ok: true, data: `Category ${category} doesn't exist`});        	    	
        }
      }
      catch(error){
        res.send({ error });
      };
    }	   

};
module.exports = new CategoryController();
