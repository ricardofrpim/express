const Products = require('../models/ProductsModel');
const Categories = require('../models/CategoriesModel');

class ProductController {

    // ADD PRODUCT
    async add (req, res) {

        let { category, name, price, color, description } = req.body;
        const foundProd = await Products.findOne({ name });
        const foundCateg = await Categories.findOne({ category });
        console.log(foundCateg)

        try {
            if(foundProd){
                res.send({ ok: true, data: `product ${name} already exists` });
            } else {
                if (!foundCateg) {
                    const addCategory = await Categories.create({ category });
                    res.send({ ok: true, data: `Category ${category} added successfully` });                    
                }
                const addProduct = await Products.create({ name, price, color, description, category});
                res.send({ ok: true, data: `product ${name} added successfully` });
            }            
          }
          catch(error){
            res.send({ error });
          };
        }


    // DELETE PRODUCT
    async delete (req, res){

        let { name } = req.body;
        const foundProd = await Products.findOne({ name });

        try {
            if(foundProd){            
                const removeProduct = await Products.deleteOne({ name });
                res.send({ ok: true, data: `product ${name} deleted successfully` });
            } else {
                res.send({ ok: true, data: `product ${name} doesn't exist` });            
            }
        }
        catch(error){
            res.send({ error });
        };
    }


    // UPDATE PRODUCT
    async update (req, res){

        let { name, price, color, description, newName, newColor, newPrice, newDes } = req.body;
        const foundProd = await Products.findOne({ name });

        try {
            if(foundProd) {
            const updateProduct = await Products.updateOne(
                { name, price, color, description },{ 
                    name: newName,
                    price: newPrice,
                    color: newColor,
                    description: newDes, 
                }
            );
                res.send({ ok: true, data: `product ${name} updated successfully` });
            } else {
                res.send({ ok: true, data: `product ${name} doesn't exist` });            
            }
        }
        catch(error){
            res.send({ error });
        };
    }


      // GET ALL PRODUCTS AND CATEGORIES
      async find (req, res){

        try {
            const products = await Products.find({});
            res.send(products);
        }
        catch(error){
            res.send({ error })
        };
    }


    // GET ONE PRODUCT BY NAME IN THE BODY
    async findOne (req ,res){

        let { product_id } = req.params;

        try {
            const oneProduct = await Products.findOne({ _id: product_id });
            res.send(oneProduct);
        }
        catch(error){
            res.send({ error })
        };

    }

};
module.exports = new ProductController();
