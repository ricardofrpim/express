const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productsSchema = new Schema({ 
    name: {type: String, required: true, unique: true},
    price: {type: Number, required: true},
    color: {type: String, required: true},
    description: {type: String, required: true, unique: true},
    category: {type: String, required: true, unique: true}
});

module.exports =  mongoose.model('products', productsSchema);