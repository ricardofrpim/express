const express = require('express');
const app = express(); 

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

const port = 4040;

app.listen(port,() => {
    console.log(`server is running on port ${port}`); 
});

var DB = [];

app.post('/category/add', (req,res)=> {
    const {category} = req.body;
    const index = DB.findIndex(x => x.category === category);
    
    if (index === -1) {
        DB.push({category, products:[]})
        res.status(200).send({ ok: true, data: `Category ${category} added successfully`});
    } else {
        res.status(200).send({ ok: true, data: `Category ${category} already exists`});        
    }

});

app.post('/category/delete', (req,res)=> {
    const {category} = req.body;
    const index = DB.findIndex(x => x.category === category);
    
    if (index !== -1) {
        DB.splice(index, 1);
        res.status(200).send({ ok: true, data: `Category ${category} deleted successfully`});
    } else {
        res.status(200).send({ ok: true, data: `Category ${category} doesn't exist`});        
    }

});

app.post('/category/update', (req,res)=> {
    const {category, newCategory} = req.body;
    const index = DB.findIndex(x => x.category === category);
    
    if (index !== -1) {
        DB[index].category = newCategory;
        res.status(200).send({ ok: true, data: `Category ${newCategory} updated successfully`});
    } else {
        res.status(200).send({ ok: true, data: `Category ${category} doesn't exist`});        
    }

});

app.get('/category/categories', (req,res)=> {
    const categories = DB.map(elem => elem.category);
        res.status(200).send({ ok: true, data: `${categories}`});
})

app.get('/category/products', (req, res) => {
        res.status(200).send({ ok: true, data: DB});
});

app.get('/category/:category', (req, res) => {
    const {category} = req.params;
    let index = DB.findIndex(x => x.category == category);

    if(index !== -1){
        res.status(200).send({ ok: true, data: DB[index].products});
    } else {
        res.status(200).send({ ok: true, data: `Category ${category} doesn't exist`});        
    }
});

app.post('/product/add', (req,res)=> {
    const {category, name, price, color, description} = req.body;
    const product = {name: name, price: price, color: color, description: description}
    let index = DB.findIndex(x => x.category == category);

    if(index !== -1) {
        let productIndex = DB[index].products.findIndex(x => x.name == product.name);
        if (productIndex === -1) {
            DB[index].products.push(product);
            res.status(200).send({ ok: true, data: `product ${name} added successfully`});
        } else {
        res.status(200).send({ ok: true, data: `Product ${name} already exists`});        
        };
    } else {
        DB.push({category, products:[product]})        
            res.status(200).send({ ok: true, data: `product ${name} added successfully`});

    }
});

app.post('/product/delete', (req,res)=> {
    const {category, name, price, color, description} = req.body;
    const product = {name: name, price: price, color: color, description: description}
    let index = DB.findIndex(x => x.category == category);

    if(index !== -1) {
        let productIndex = DB[index].products.findIndex(x => x.name == product.name);
        if (productIndex !== -1) {
            DB[index].products.splice(productIndex, 1);
            res.status(200).send({ ok: true, data: `product ${name} deleted successfully`});
        }
    }
});

app.post("/product/update", (req, res) => {
    const { category, newName, newPrice, newColor, newDescription, name, price, color, description } = req.body;
    var product = { name: name, price: price, color: color, description: description };
    var index = DB.findIndex(x => x.category == category);

    if(index !== -1){
        var productIndex = DB[index].products.findIndex(x => x.name == product.name);
        if(productIndex !== -1){
            DB[index].products[productIndex].name = newName;
            DB[index].products[productIndex].price = newPrice;
            DB[index].products[productIndex].color = newColor;
            DB[index].products[productIndex].description = newDescription;
            res.status(200).send({ ok: true, data: `product ${newName} updated successfully`});            
            res.send(DB[index]);
        } else {
            res.status(200).send({ ok: true, data: `Product ${name} doesn't exist`});        
        }
    } else {
        res.status(200).send({ ok: true, data: `Category ${category} doesn't exist`});                
    }
});



